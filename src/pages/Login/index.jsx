import React from 'react'
import Login from '../../components/Forms/Login'

const LoginPage = () => {
  return (
    <div className='full-parent-height flex-column'>
      <Login />
    </div>
  )
}

export default LoginPage
