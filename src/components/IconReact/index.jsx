import React from 'react'
import './style.scss'

const IconReact = ({ icon, text, onClick }) => {
  return (
    <div className="icon-re" onClick={onClick}>
      {icon}
      {text && (<span className="icon-re__text">{text}</span>)}
    </div>
  )
}

export default IconReact
