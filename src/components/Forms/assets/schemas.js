import * as yup from 'yup'

import { emailRegex, phoneRegex } from './regexp'
import { Translation } from 'react-i18next'

export const loginSchema = yup.object({
  login: yup
    .string()
    .required(
      <Translation>
        {(t, { i18n }) => t('form.errors.isFieldRequired')}
      </Translation>
    )
    .min(5, <Translation>
      {(t, { i18n }) => `5 ` + t('form.errors.minimum')}
    </Translation>,)
     .max(20, <Translation>
      {(t, { i18n }) => `20 ` + t('form.errors.maximum')}
    </Translation>,)
    .test(
      'test-name',
      <Translation>
        {(t, { i18n }) => t('form.errors.isEmailPhone')}
      </Translation>,
      function (value) {
        let isValidEmail = emailRegex.test(value)
        let isValidPhone = phoneRegex.test(value)
        if (!isValidEmail && !isValidPhone) {
          return false
        }
        return true
      }
    ),
  password: yup
    .string()
    .required(
      <Translation>
        {(t, { i18n }) => t('form.errors.isFieldRequired')}
      </Translation>
    ),
}) //.required();
