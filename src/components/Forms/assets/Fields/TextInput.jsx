import React from 'react'
import { useFormContext } from 'react-hook-form'

const TextInput = ({
  name,
  type="text",
  label = '',
  subText = '',
  placeholder = '',
}) => {
  const {
    register,
    formState: { errors },
  } = useFormContext()

  return (
    <div className="form__row">
      {label && (
        <label className="form__label" htmlFor={name}>
          {label}
        </label>
      )}
      <input
        className={`form__field ${
          errors[name]?.message ? 'form_error_field' : ''
        } `}
        type={type}
        name={name}
        id={name}
        {...register(name)}
        placeholder={placeholder}
      />
      {subText && <p className="form__subtext">{subText}</p>}
      {errors[name]?.message && (
        <p className="form__error">{errors[name].message}</p>
      )}
    </div>
  )
}

export default TextInput
