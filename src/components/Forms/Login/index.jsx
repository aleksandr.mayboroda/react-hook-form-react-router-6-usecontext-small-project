import React from 'react'
import { useForm, FormProvider } from 'react-hook-form'
import useAuth from '../../../hooks/useAuth'
import { yupResolver } from '@hookform/resolvers/yup'
import { loginSchema } from '../assets/schemas'

import '../style.scss'

import Logo from '../../Logo'
import TextInput from '../assets/Fields/TextInput'

import { useTranslation } from 'react-i18next'

const emptyValues = {
  login: '',
  password: '',
}

const Login = () => {
  const { doLogin } = useAuth()

  const methods = useForm({
    defaultValues: emptyValues,
    resolver: yupResolver(loginSchema),
  })

  const { t } = useTranslation()

  const onSubmit = async (formData) => {
    try {
      const isLoggedIn = await doLogin(formData)
      if (isLoggedIn) {
        methods.reset(emptyValues)
      }
      methods.setError('server', {
        type: 'manual',
        message: t('form.errors.isLogin'),
      })
    } catch (er) {
      methods.setError('server', {
        type: 'manual',
        message: t('form.errors.isServer'),
      })
    }
  }
  return (
    <div className="form">
      <Logo />
      <h3 className="form__title">{t('form.titles.login')}</h3>
      <FormProvider {...methods}>
        <form method="post" action="" onSubmit={methods.handleSubmit(onSubmit)}>
          <TextInput
            name="login"
            label={t('form.fields.login.label')}
            subText={t('form.fields.login.subText')}
            placeholder={t('form.fields.login.placeHolder')}
          />
          <TextInput
            name="password"
            type="password"
            label={t('form.fields.password.label')}
            placeholder={t('form.fields.password.placeHolder')}
          />

          <div className="form__row">
            {methods?.errors?.server?.message && (
              <p className="form__error">{methods.errors.server.message}</p>
            )}
            <button className="form__btn form_form_submit">
              {t('form.buttons.submit')}
            </button>
          </div>
        </form>
      </FormProvider>
    </div>
  )
}

export default Login
