import * as Icons from '../../ui/icons'

const Icon = ({type, className = '', onClick, ...rest}) => {
  const iconJsx = Icons[type]

  if(!iconJsx)
  {
    return null
  }

  return (
    <span className={className} onClick={onClick}>
      {iconJsx({...rest})}
    </span>
  )
}

export default Icon