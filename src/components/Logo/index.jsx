import React from 'react'
import Icon from '../Icon'
import {Link} from 'react-router-dom'

const Logo = () => {
  return (
    <Link className="logo" to='/'>
      <Icon type="logo" width={'170px'} />
    </Link>
  )
}

export default Logo
