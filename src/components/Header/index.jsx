import React from 'react'
import './style.scss'

import HeaderLeft from './HeaderLeft'
import HeaderRight from './HeaderRight'


const Header = () => {
  return (
    <header className="header">
      <div className="container">
        <div className="header__container">
          <HeaderLeft />
          <HeaderRight />
        </div>
      </div>
    </header>
  )
}

export default Header
