import { memo } from 'react'
import {useNavigate} from 'react-router-dom'
import { useContext } from 'react'
import { UserContext } from '../../context/userContext'

import { FaUserCircle, FaMagic, FaUserCog, FaUserTimes } from 'react-icons/fa'
import IconReact from '../IconReact'

const HeaderRight = () => {
  const userContext = useContext(UserContext)

  let userIcons = null
  if(!userContext.userData)
  {
    userIcons = (<IconReact icon={<FaUserCircle onClick={() => navigate('/login')} />} />)
  }
  else
  {
    userIcons = (
      <>
        <IconReact icon={<FaUserCog onClick={() => navigate('/profile')} />} />
        <IconReact icon={<FaUserTimes onClick={() => userContext.clearContext()}/>} />
      </>
    )
  }

  const navigate = useNavigate()
  return (
    <div className="header__right">
      {/* {!userContext.userData && (<IconReact icon={<FaUserCircle onClick={() => navigate('/login')} />} />)}
      {userContext.userData && (<IconReact icon={<FaUserCog onClick={() => navigate('/profile')} />} />)
      <IconReact icon={<FaUserTimes />} /> */}
      {userIcons}
      <IconReact icon={<FaMagic />} />
    </div>
  )
}

export default memo(HeaderRight)
