import {memo, useState, useEffect} from 'react'

import { FaLocationArrow , FaGlobe} from 'react-icons/fa'
import IconReact from '../IconReact'
import Logo from '../Logo'

import { useTranslation } from 'react-i18next'

const HeaderLeft = () => {
  const {i18n} = useTranslation()
  const city = 'kyiv'
  const [language,setLanguage] = useState('en')
  const changeLanguage = () => setLanguage(prev => prev === 'en' ? 'ua' : 'en')

  useEffect(() => {
    i18n.changeLanguage(language)
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[language])

  return (
    <div className="header__left">
      <Logo />
      <IconReact icon={<FaLocationArrow />} text={city} />
      <IconReact icon={<FaGlobe />} text={language} onClick={changeLanguage} />
    </div>
  )
}

export default memo(HeaderLeft)
