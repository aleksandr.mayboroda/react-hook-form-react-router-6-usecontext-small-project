import React from 'react'
import './style.scss'

const Footer = () => {
  const date = new Date()
  return (
    <footer className="footer">
      <p className="footer__date">&copy; {date.getFullYear()}</p>
    </footer>
  )
}

export default Footer
