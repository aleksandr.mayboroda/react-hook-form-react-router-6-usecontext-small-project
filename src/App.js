import AppRoutes from './routes/AppRoutes'
import Header from './components/Header'
import Footer from './components/Footer'


function App() {
  return (
    <>
      <Header />
      <main>
        <AppRoutes />
      </main>
      <Footer />
    </>
  )
}

export default App
