import { useContext } from 'react'
import { UserContext } from '../../context/userContext'
import { useNavigate, useLocation } from 'react-router-dom'
import axios from 'axios'

const useAuth = () => {
  const userContext = useContext(UserContext)

  const navigate = useNavigate()
  const location = useLocation()

  const doLogin = async (formData) => {
    const usersData = await axios.get('/users.json')

    const isExists = usersData.data.find(
      (usr) =>
        (usr.email === formData.login || usr.phone === formData.login) &&
        usr.password === formData.password
    )
    if (isExists) {
      userContext.setUserData(isExists)
      if (location.state?.from) {
        navigate(location.state.from)
        location.state = null
      }
      navigate('/profile')
      return isExists
    }
    return false
  }

  const doRegister = (formData) => {
    /// ...here...
    return false
  }

  return {
    doLogin,
    doRegister,
  }
}

export default useAuth
