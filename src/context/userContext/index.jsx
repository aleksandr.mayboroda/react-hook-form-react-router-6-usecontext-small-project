import { createContext, useState } from 'react'

export const initialValue = {
  user: null,
}

export const UserContext = createContext(initialValue)

export const UserProvider = ({ children }) => {
  const [userData, setUserData] = useState(initialValue.user)
  const clearContext = () => setUserData(null)
  return (
    <UserContext.Provider value={{ userData, setUserData, clearContext }}>
      {children}
    </UserContext.Provider>
  )
}
