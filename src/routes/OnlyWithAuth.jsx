import React from 'react';
import { Navigate , useLocation, Outlet } from 'react-router-dom';
import { useContext } from 'react'
import { UserContext } from '../context/userContext'

const OnlyWithAuth = ({reLocateTo = '/'}) => {
  const userContext = useContext(UserContext)

  const location = useLocation()
  if(!userContext.userData)
  {
    return <Navigate to={reLocateTo} state={{ from: location.pathname }} />;
  }
  return (
    <Outlet />
  )
};

export default OnlyWithAuth;
