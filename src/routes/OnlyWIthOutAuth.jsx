import { Navigate, Outlet } from 'react-router-dom'
import { useContext } from 'react'
import { UserContext } from '../context/userContext'

const OnlyWIthOutAuth = ({ reLocateTo = '/' }) => {
  const userContext = useContext(UserContext)

  if (userContext.userData) {
    return <Navigate to={reLocateTo} />
  }
  return <Outlet />
}

export default OnlyWIthOutAuth
