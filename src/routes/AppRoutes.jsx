import React from 'react'
import { Routes, Route } from 'react-router-dom'
import Home from '../pages/Home'
import LoginPage from '../pages/Login'
import UserProfile from '../pages/UserProfile'
import OnlyWithAuth from './OnlyWithAuth'
import OnlyWIthOutAuth from './OnlyWIthOutAuth'

const AppRoutes = () => {
  return (
    <Routes>
      <Route path="/" element={<Home />} />

      <Route element={<OnlyWIthOutAuth reLocateTo="/" />}>
        <Route path="/login" element={<LoginPage />} />
      </Route>

      {/* TO make this protected */}
      <Route element={<OnlyWithAuth rule={false} reLocateTo="/login" />}>
        <Route path="/profile" element={<UserProfile />} />
      </Route>
    </Routes>
  )
}

export default AppRoutes
